#!/usr/bin/env bash

sudo dnf copr enable evana/fira-code-fonts







sudo dnf install -y python3-devel
sudo dnf install -y python2-devel
sudo dnf install -y python3-pip
sudo dnf install -y python2-pip
sudo dnf install -y golang
sudo dnf install -y gcc
sudo dnf install -y cmus
sudo dnf install -y fira-code
sudo dnf install -y zsh
sudo dnf install -y vim
sudo dnf install -y git
sudo dnf install -y arandr
sudo dnf install -y groupupdate multimedia
sudo dnf install -y groupupdate sound-and-video
sudo dnf install -y kernel-devel xorg-x11-drv-nvidia akmod-nvidia xorg-x11-drv-nvidia-cuda
sudo dnf install -y vdpauinfo libva-vdpau-driver libva-utils ffmpeg
sudo dnf install -y libreoffice
sudo dnf install -y steam
sudo dnf install -y lutris

# install doom emacs and spacevim
cd Downloads
git clone https://github.com/hlissner/doom-emacs.git ~/.emacs.d
git checkout develop
cd bin
./doom quickstart

cd ~

curl -sLf https://spacevim.org/install.sh | bash

# Some programs and stuff
mkdir programs
cd programs
git clone https://github.com/tobi-wan-kenobi/bumblebee-status.git
cd ~



