;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
'("melpa" . "https://melpa.org/packages/"))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
(package-refresh-contents)
(package-install 'use-package))

(use-package try
:ensure t)

(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1) 

(defalias 'yes-or-no-p 'y-or-n-p)

(use-package smart-mode-line-powerline-theme
   :ensure t
   :after powerline
   :after smart-mode-line
   :config
    (sml/setup)
    (sml/apply-theme 'powerline)
    )

(add-hook 'org-mode-hook 'org-indent-mode)


(use-package dashboard
    :ensure t
    :diminish dashboard-mode
    :config
    (setq dashboard-banner-logo-title "I bless your computer my child")
    (setq dashboard-startup-banner "/home/csreindeer/Pictures/kisspng-richard-stallman-computer-software-programmer-gnu-emacs-5b2613cdd883c7.3959239615292220938869.png")
    (setq dashboard-items '((recents  . 10)
                            (bookmarks . 10)))
    (dashboard-setup-startup-hook))

(global-unset-key (kbd "<left>"))
(global-unset-key (kbd "<right>"))
(global-unset-key (kbd "<up>"))
(global-unset-key (kbd "<down>"))
(global-unset-key (kbd "<C-left>"))
(global-unset-key (kbd "<C-right>"))
(global-unset-key (kbd "<C-up>"))
(global-unset-key (kbd "<C-down>"))
(global-unset-key (kbd "<M-left>"))
(global-unset-key (kbd "<M-right>"))
(global-unset-key (kbd "<M-up>"))
(global-unset-key (kbd "<M-down>"))

(use-package atom-one-dark-theme
  :config
  (load-theme 'atom-one-dark t))

  (set-frame-font "Fira Code 11" nil t)

  (use-package smartparens
    :ensure t
    :diminish smartparens-mode
    :config
    (add-hook 'prog-mode-hook 'smartparens-mode))

  (use-package rainbow-delimiters
    :ensure t
    :config
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))


  (use-package rainbow-mode
    :ensure t
    :config
    (setq rainbow-x-colors nil)
    (add-hook 'prog-mode-hook 'rainbow-mode))

 (use-package aggressive-indent
	      :ensure t)

(add-hook 'prog-mode-hook 'electric-pair-mode)

  (use-package magit
    :ensure t
    :bind ("C-x g" . magit-status))

  (use-package git-gutter
    :ensure t
    :config
    (global-git-gutter-mode 't)
    :diminish git-gutter-mode)

  (use-package git-timemachine
	       :ensure t)


  (use-package web-mode
    :ensure t
    :mode ("\\.html\\'")
    :config
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-engines-alist
          '(("django" . "focus/.*\\.html\\'")
            ("ctemplate" . "realtimecrm/.*\\.html\\'"))))

(use-package swift-mode
  :ensure t
  )

  (use-package markdown-mode
    :ensure t
    :commands (markdown-mode gfm-mode)
    :mode (("README\\.md\\'" . gfm-mode)
           ("\\.md\\'" . markdown-mode)
           ("\\.markdown\\'" . markdown-mode))
    :init (setq markdown-command "multimarkdown"))

  (use-package go-mode
    :ensure t
    :config
    (add-hook 'before-save-hook 'gofmt-before-save)

    (use-package go-eldoc
      :ensure t
      :config
      (add-hook 'go-mode-hook 'go-eldoc-setup))

    (use-package godoctor
      :ensure t)

    (use-package go-guru
		 :ensure t))

(use-package elpy
  :ensure t
  :init
  (elpy-enable))

(use-package irony
  :ensure t
  :hook (c-mode . irony-mode))

(use-package org-bullets
:ensure t
:init
(setq org-bullets-bullet-list
'("◉" "◎" "⚫" "○" "►" "◇"))
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))


;; load evil
(use-package evil
  :ensure t ;; install the evil package if not installed
  :init ;; tweak evil's configuration before loading it
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll t)
  :config ;; tweak evil after loading it
  (evil-mode)

  ;; example how to map a command in normal mode (called 'normal state' in evil)
  (define-key evil-normal-state-map (kbd ", w") 'evil-window-vsplit))


    (use-package writegood-mode
    :ensure t
    :bind ("C-c g" . writegood-mode)
    :config
    (add-to-list 'writegood-weasel-words "actionable"))

    
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("44961a9303c92926740fc4121829c32abca38ba3a91897a4eab2aa3b7634bed4" "50ff65ab3c92ce4758cc6cd10ebb3d6150a0e2da15b751d7fbee3d68bba35a94" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "9f08dacc5b23d5eaec9cccb6b3d342bd4fdb05faf144bdcd9c4b5859ac173538" "0f1733ad53138ddd381267b4033bcb07f5e75cd7f22089c7e650f1bb28fc67f4" "37c5cf50a60548aa7e01dbe36fd8bb643af7502d55d26f000070255a6b21c528" "bf39b2d814971a6eaf4e9adde3b11016b742fe68dfe4c38667497821525a1662" "8abc02cbf62cc9efb0e571233387866b9b26c3c4e8ab75148b502f0646b46225" "79dc2bcd864232143f961d46454c2252bb705b691967b61558e038e8497ff2e5" "36c3457b6b364cc93f2d52e2d9b9ec32bab75b27f5bd2394757657f18f4d324e" "8be07a2c1b3a7300860c7a65c0ad148be6d127671be04d3d2120f1ac541ac103" "3380a2766cf0590d50d6366c5a91e976bdc3c413df963a0ab9952314b4577299" "fb44ced1e15903449772b750c081e6b8f687732147aa43cfa2e7d9a38820744b" "d96587ec2c7bf278269b8ec2b800c7d9af9e22d816827639b332b0e613314dfd" "7bef2d39bac784626f1635bd83693fae091f04ccac6b362e0405abf16a32230c" "9be1d34d961a40d94ef94d0d08a364c3d27201f3c98c9d38e36f10588469ea57" "ecfd522bd04e43c16e58bd8af7991bc9583b8e56286ea0959a428b3d7991bbd8" "80930c775cef2a97f2305bae6737a1c736079fdcc62a6fdf7b55de669fbbcd13" "93268bf5365f22c685550a3cbb8c687a1211e827edc76ce7be3c4bd764054bad" "8cf1002c7f805360115700144c0031b9cfa4d03edc6a0f38718cef7b7cabe382" "c968804189e0fc963c641f5c9ad64bca431d41af2fb7e1d01a2a6666376f819c" "4b2679eac1095b60c2065187d713c39fbba27039d75c9c928a1f3b5d824a3b18" "6145e62774a589c074a31a05dfa5efdf8789cf869104e905956f0cbd7eda9d0e" "ea9e9f350c019474a5265c08f7441027b23c1da3f23b9c30517d60133bab679f" "cea3ec09c821b7eaf235882e6555c3ffa2fd23de92459751e18f26ad035d2142" "cbd8e65d2452dfaed789f79c92d230aa8bdf413601b261dbb1291fb88605110c" "aea30125ef2e48831f46695418677b9d676c3babf43959c8e978c0ad672a7329" "f6f5d5adce1f9a764855c9730e4c3ef3f90357313c1cae29e7c191ba1026bc15" "986e7e8e428decd5df9e8548a3f3b42afc8176ce6171e69658ae083f3c06211c" "65f35d1e0d0858947f854dc898bfd830e832189d5555e875705a939836b53054" "7a1190ad27c73888f8d16142457f59026b01fa654f353c17f997d83565c0fc65" "350dc341799fbbb81e59d1e6fff2b2c8772d7000e352a5c070aa4317127eee94" "dd4628d6c2d1f84ad7908c859797b24cc6239dfe7d71b3363ccdd2b88963f336" "5b8eccff13d79fc9b26c544ee20e1b0c499587d6c4bfc38cabe34beaf2c2fc77" "ffe80c88e3129b2cddadaaf78263a7f896d833a77c96349052ad5b7753c0c5a5" "8543b328ed10bc7c16a8a35c523699befac0de00753824d7e90148bca583f986" "819d24b9aba8fcb446aecfb59f87d1817a6d3eb07de7fdec67743ef32194438b" "f66abed5139c808607639e5a5a3b5b50b9db91febeae06f11484a15a92bde442" "e1498b2416922aa561076edc5c9b0ad7b34d8ff849f335c13364c8f4276904f0" "fede08d0f23fc0612a8354e0cf800c9ecae47ec8f32c5f29da841fe090dfc450" "527df6ab42b54d2e5f4eec8b091bd79b2fa9a1da38f5addd297d1c91aa19b616" "4bf5c18667c48f2979ead0f0bdaaa12c2b52014a6abaa38558a207a65caeb8ad" "df21cdadd3f0648e3106338649d9fea510121807c907e2fd15565dde6409d6e9" "d9dab332207600e49400d798ed05f38372ec32132b3f7d2ba697e59088021555" "264b639ee1d01cd81f6ab49a63b6354d902c7f7ed17ecf6e8c2bd5eb6d8ca09c" "196df8815910c1a3422b5f7c1f45a72edfa851f6a1d672b7b727d9551bb7c7ba" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "722e1cd0dad601ec6567c32520126e42a8031cd72e05d2221ff511b58545b108" "446cc97923e30dec43f10573ac085e384975d8a0c55159464ea6ef001f4a16ba" "b3bcf1b12ef2a7606c7697d71b934ca0bdd495d52f901e73ce008c4c9825a3aa" "7220c44ef252ec651491125f1d95ad555fdfdc88f872d3552766862d63454582" "7559ac0083d1f08a46f65920303f970898a3d80f05905d01e81d49bb4c7f9e39" "0c3b1358ea01895e56d1c0193f72559449462e5952bded28c81a8e09b53f103f" "bf5bdab33a008333648512df0d2b9d9710bdfba12f6a768c7d2c438e1092b633" default)))
 '(package-selected-packages
   (quote
    (evil color-theme-sanityinc-tomorrow kaolin-themes smart-mode-line-powerline-theme atom-one-dark-theme elpy p writegood-mode web-mode use-package try smartparens rainbow-mode rainbow-delimiters org-bullets markdown-mode magit irony godoctor go-guru go-eldoc git-timemachine git-gutter aggressive-indent))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight normal :height 107 :width normal)))))

